# Graph Vertex Coloring Problem (GVCP) 
# Findig the minimum number of colors
# chromatic number χ (G)
# How to run:
# julia graph_coloring.jl input4.txt

using DelimitedFiles
using DataStructures

function readFile(filename::String)::Tuple{Int32, Int32, Vector{Vector{Any}}}

    input = []
    open(filename) do f
        input = readdlm(f, Int64)
    end

    n::Int32 = input[1, 1]
    m::Int32 = input[1, 2]
    graph = [[]::Vector{Any} for _ in 1:n]::Vector{Vector{Any}}

    for i in 2:(m + 1)
        v::Int32 = input[i, 1][1]
        u::Int32 = input[i, 2][1]

        # Self loops are not allowed
        if v == u
            continue
        end

        push!(graph[u], v)
        push!(graph[v], u)
    end

    return n, m, graph
end

function F(s::Dict{Int32, Int32}, graph::Vector{Vector{Any}})::Tuple{Int32, Vector{Int32}}
    sum = 0
    move_candidates = Set{Int32}()
    
    for i in eachindex(graph)
        for j in graph[i]
            
            if s[i] == s[j]
                push!(move_candidates, i)
                push!(move_candidates, j)
                sum += 1;
            end
        end
    end

    return sum, collect(move_candidates)
end

function TabuCol(solution::Dict{Int32, Int32}, iterTC::Int32, k::Int32, graph::Vector{Vector{Any}}, reps = 100, tabu_size = 50)::Dict{Int32, Int32}
    tabu::Vector{Tuple{Int32, Int32}} = []
    aspirationLevel::Dict{Int32, Int32} = Dict()
    newSolution::Dict{Int32, Int32} = Dict()

    nbiter::Int32 = 0;
    while nbiter < iterTC
        node = 0
        conflicts, moveCandidates = F(solution, graph)

        if conflicts == 0
            # Valid coloring
            break
        end

        for _ in 1:reps
            node = moveCandidates[rand(1:length(moveCandidates))]
            newColor::Int32 = rand(1:k)
            if get(solution, node, -1) == newColor
                newColor = rand(1:k)
            end

            newSolution = copy(solution)
            newSolution[node] = newColor

            newConflicts, _ = F(newSolution, graph)
            if newConflicts < conflicts
                 # We got a better solution
                if newConflicts <= get!(aspirationLevel, conflicts, conflicts - 1)
                    aspirationLevel[conflicts] = newConflicts - 1

                    if (node, newColor) in tabu
                        filter!(x -> x != (node, newColor), tabu)
                    end                        
                elseif (node, newColor) in tabu
                    # tabu move isn't good enough
                    continue
                end

                break
            end
        end

        push!(tabu, (node, solution[node]))
        if length(tabu) > tabu_size
            pop!(tabu)
        end 

        solution = newSolution
        nbiter += 1
    end

    return solution
end

function getBiggestPartitionColor(p::Dict{Int32, Int32}, k::Int32, chosenColors::Set{Int32})::Int32
    if isempty(p)
        return 0
    end

    list = [0 for _ in 1:k]

    for (_, color) in p
        if color ∉ chosenColors
            list[color] += 1
        end
    end

    return findmax(list)[2]
end

function GPX(s1::Dict{Int32, Int32}, s2::Dict{Int32, Int32}, k::Int32)::Dict{Int32, Int32}
    p1 = copy(s1)
    p2 = copy(s2)
    chosenColors = Set{Int32}()
    newGene = Dict{Int32, Int32}()
    for _ in 1:k
        # The first step is to transmit to the child the largest color class of the first parent.
        # If there areseveral largest color classes, one of them is chosen at random.    
        p1_color = getBiggestPartitionColor(p1, k, chosenColors)
        for (node, color) in p1
            if p1_color == color
                newGene[node] = color
                delete!(p1, node)
                delete!(p2, node)
            end
        end
        push!(chosenColors, p1_color)

        # After having withdrawn
        # those vertices in the second parent, one proceeds to step 2 where one transmits to the
        # child the largest color class of the second parent. 
        p2_color = getBiggestPartitionColor(p2, k, chosenColors)
        for (node, color) in p2
            if p2_color == color
                newGene[node] = color
                delete!(p1, node)
                delete!(p2, node)
            end
        end
        push!(chosenColors, p2_color)
    end

    chosenColors = collect(chosenColors)
    # This process is repeated until all
    # the colors are used. There are most probably still some uncolored vertices in the child
    # solution. The final step (step k+1) is to randomly add those vertices to the color classes.
    for (remainedNode, _) in p1
        newGene[remainedNode] = rand(1:k)
        delete!(p1, remainedNode)
        delete!(p2, remainedNode)
    end

    for (remainedNode, _) in p2
        newGene[remainedNode] = rand(1:k)
        delete!(p1, remainedNode)
        delete!(p2, remainedNode)
    end

    return newGene
end

function saveBest(solution1, solution2, graph)::Dict{Int32, Int32}
    f1, _ = F(solution1, graph)
    f2, _ = F(solution2, graph)

    if f1 <= f2
        return solution1
    else f2 < f1
        return solution2
    end
end

function saveBest(solution1::Dict{Int32, Int32}, solution2::Dict{Int32, Int32}, 
    solution3::Dict{Int32, Int32}, graph::Vector{Vector{Any}})::Dict{Int32, Int32}
    f1, _ = F(solution1, graph)
    f2, _ = F(solution2, graph)
    f3, _ = F(solution3, graph)

    if f1 <= f2 && f1 <= f3
        return solution1
    elseif f2 <= f1 && f2 <= f3
        return solution2
    else f3 <= f1 && f3 <= f2
        return solution3
    end
end

function init(n::Int32, k::Int32)::Dict{Int32, Int32}
    d = Dict{Int32, Int32}()
    for i in 1:n
        d[i] = rand(1:k)
    end

    return d
end

function HEAD_alg(n::Int32, graph::Vector{Vector{Any}}, k::Int32)::Dict{Int32, Int32}
    iterTC::Int32 = 10^3
    iterCycle::Int32 = 10
    generation::Int32 = 0

    p1 = Dict{Int32, Int32}()
    p2 = Dict{Int32, Int32}()
    best = Dict{Int32, Int32}()
    elite1 = Dict{Int32, Int32}()
    elite2 = Dict{Int32, Int32}()


    for i in 1:n
        p1[i] = rand(1:k)
        p2[i] = rand(1:k)
        best[i] = rand(1:k)
        elite1[i] = rand(1:k)
        elite2[i] = rand(1:k)
    end

    i::Int64 = 0
    while (true)
        c1 = GPX(p1, p2, k)
        c2 = GPX(p2, p1, k)        

        p1 = TabuCol(c1, iterTC, k, graph)
        p2 = TabuCol(c2, iterTC, k, graph)

        elite1 = saveBest(p1, p2, elite1, graph)
        
        best = saveBest(elite1, best, graph)

        if generation % iterCycle == 0
            p1 = elite2
            elite2 = elite1
            elite1 = init(n, k)
        end
        generation += 1

        bestConflicts, _ = F(best, graph)
        println("iteracio: ", i, " number of conflicts: ", bestConflicts)
        if bestConflicts == 0
            println("Kilehet szinezni a grafot legalabb k: ", k, " szinnel!")
            break
        end
        if  p1 == p2
            println("Hiba tortent! A ket gen megegyezik!")
            break
        end

        i += 1
    end

    return best
end

function main()
    if (length(ARGS) != 1)
        println("Kerek egy input fajlt!")
        return
    end
    
    n, m, graph = readFile(ARGS[1])
    
    k::Int32 = 3
    solution = HEAD_alg(n, graph, k)
    solution = sort(collect(solution), by = x->x[1])

    println(k)
    for (_, color) in solution
        print(color, " ")
    end
    println()
end

main()
